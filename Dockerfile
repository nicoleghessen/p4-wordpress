# We want a wordpress server

# Get a base container with wordpress installed
FROM wordpress:4.9.5

# Explicitly mark port 80 as exposed
EXPOSE 80