# gives user acces to use kubectl 
sudo cp /etc/kubernetes/admin.conf $HOME/ && sudo chown $(id -u):$(id -g) $HOME/admin.conf && export KUBECONFIG=$HOME/admin.conf

# creates namespace for wordpress and deploys it on there
kubectl create ns wordpress
kubectl apply -k ./ -n wordpress