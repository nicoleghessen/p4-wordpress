# Wordpress 

Welcome to the wordpress project, here are instructions to launch and manage your wordpress

## Prerequisites
Make sure you have cloned this repo
To use wordpress the following machines must be running on aws ec2 (in ireland as the academy profile):
* jsn-wordpress-kubernetes-control
* jsn-wordpress-kubernetes-node
* jsn-jenkins-master-node
* jsn-jenkins-agent-node

__For your convienience you can enter `team=jsn` in the ec2 services search bar to see all of these machines.__

## Starting Wordpress
Check if wordpress is already running on the link: `http://james.nginx.academy.labs.automationlogic.com/`
If not, visit the jenkins server at `http://jsn-jenkins.academy.labs.automationlogic.com:8080/` and run the `Wordpress-dev-cd` job. After it has built revisit the website to see wordpress.

## Stopping Wordpress
Visit the jenkins server at `http://jsn-jenkins.academy.labs.automationlogic.com:8080/` and start the `Wordpress-dev-stopper` job. After it has build revisit the website and it will no longer be accessible

## Changing the number of deployment pods
make sure you are in the dev branch
open `wordpress-mysql-deployment.yaml` and change line 11 so the repica is the value of pods you want
```bash
11| replicas: n # where n is the number of pods you want
```

Save the file then run the following commands
```
$ git add .
$ git commit -m "changing pod deployment value"
$ git push
```

Check the slack channel to see when you your changes have been carried out with the following message:

__Wordpress-dev-cd - #12 Success after X sec__

Your deployment will now be running with n amount of pods.

For further questions and the login details to wordpress please contact: `james.lok@automationlogic.com`